#!/usr/bin/env bash

# Repository
zypper --non-interactive ar -cfp 90 http://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Leap_15.1/ packman

# Packages
zypper --non-interactive in python ffmpeg \
gstreamer-libnice gstreamer-plugins-bad gstreamer-plugins-bad-orig-addon gstreamer-plugins-ugly-orig-addon gstreamer-plugins-libav

# User account skeleton
rm -rf /etc/skel
cp -r skel /etc/skel

# Main program
mkdir -p /opt/videocapture/Videocapture-x86_64.AppImage.config
cp Videocapture-x86_64.AppImage /opt/videocapture
cp settings.edn /opt/videocapture/Videocapture-x86_64.AppImage.config/
cp videocopy.bash /opt/videocapture

chown -R root:'domain users' /opt/videocapture
chmod -R 777 /opt/videocapture

# Disable C6 sleep state
cd disable-c6
./install.sh
cd ..
echo "msr" > /etc/modules-load.d/msr.conf

# Install upload service
mkdir -p /usr/local/lib/systemd/system
cp uploaderservice/* /usr/local/lib/systemd/system
systemctl enable vc-upload-watcher.path

# Upload directory
mkdir -p /srv/upload
chown -R root:'domain users' /srv/upload
chmod -R 777 /srv/upload

# Upload cert
mkdir /root/.ssh
cp ssh/* /root/.ssh
chmod 700 /root/.ssh/id_*

# Adjust login manager
cat << EOF > /usr/lib/sddm/sddm.conf.d/10-theme.conf
[Theme]
Current=breeze
CursorTheme=breeze_cursors
EOF

cat << EOF >> /usr/lib/sddm/sddm.conf.d/00-general.conf
[General]
InputMethod=qtvirtualkeyboard

[Users]
RememberLastUser=false
EOF
