#!/usr/bin/env bash
cd /srv/upload
sleep 40
scp *.webm Administrator@psy-ptpvideo.unibe.ch:"/Volumes/VTrak/Watch\ Folder/Output"
rm -f *.webm
