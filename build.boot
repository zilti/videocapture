(set-env!
 :source-paths #{"src" "test/clojure"}
 :resource-paths #{"resources"}
 :dependencies '[[org.clojure/clojure "1.10.1"]
                 [org.slf4j/slf4j-api "1.7.28"]
                 [org.openjfx/javafx-base "14-ea+1" :scope "provided"]
                 [org.openjfx/javafx-controls "14-ea+1" :scope "provided"]
                 [org.openjfx/javafx-fxml "14-ea+1" :scope "provided"]
                 [org.openjfx/javafx-media "14-ea+1" :scope "provided"]
                 [org.openjfx/javafx-swing "14-ea+1" :scope "provided"]
                 [clojurefx "0.5.1" :exclusions [org.clojure/tools.reader]]
                 [org.freedesktop.gstreamer/gst1-java-core "0.9.3"]
                 [net.java.dev.jna/jna "5.4.0"]
                 [eu.agno3.jcifs/jcifs-ng "2.1.3"]
                 [com.draines/postal "2.0.3"]
                 [clojure.java-time "0.3.2"]
                 [mount "0.1.16"]
                 [tolitius/mount-up "0.1.2"]
                 [com.taoensso/timbre "4.10.0" :exclusions [org.clojure/tools.reader]]
                 [com.fzakaria/slf4j-timbre "0.3.14"]
                 [org.clojure/core.async "0.4.500" :exclusions [org.ow2.asm/asm-all]]
                 [de.jensd/fontawesomefx-commons "9.1.2"]
                 [org.controlsfx/controlsfx "11.0.0"]

                 [marginalia "0.9.1" :scope "test"]
                 [it.frbracch/boot-marginalia "0.1.3-1" :scope "test"]
                 [adzerk/bootlaces "0.1.13" :scope "test"]
                 [com.threerings/getdown "1.7.1" :scope "test"]
                 [adzerk/bootlaces "0.2.0" :scope "test"]
                 [adzerk/boot-test "1.2.0" :scope "test"]
                 [sparkfund/boot-lein "0.4.0" :scope "test"]
                 [org.martinklepsch/boot-deps "0.1.10" :scope "test"]])

(require '[adzerk.bootlaces :refer :all])
(require '[clojure.string :as str])
(require '[clojure.java.shell :as sh])
(require '[clojure.java.io :as io])
(require '[adzerk.boot-test :as boot-test])
(require '[sparkfund.boot-lein :refer :all])
(require '[it.frbracch.boot-marginalia :refer [marginalia]])
(require 'boot.repl)

(def +version+ "1.1.4")
(bootlaces! +version+)

(task-options!
 pom {:project 'ch.unibe.psy/videocapture
      :version +version+
      :description "A video capturing application for research purposes."
      :url "http://tpf.fluido.as:10012/PTP/videocapture"
      :scm {:url "http://tpf.fluido.as:10012/PTP/videocapture.git"}
      :license {"name" "GNU General Public License"
                "url" "http://www.gnu.org/licenses/gpl.txt"}}
 jar {:main 'videocapture.core
      :file "videocapture.jar"
      :manifest {"App-Version" +version+}}
 uber {:exclude #{#"module-info\.class" #".\.SF" #".\.DSA" #".\.RSA"}})

(defn fpath [filename]
  (.toPath (io/file filename)))

(deftask run
  []
  (comp
   (repl)
   (javac)
   (with-pass-thru _
     (require 'videocapture.core)
     ((resolve 'videocapture.core/-main)))))

(deftask test
  "Run the videocapture automated tests."
  []
  (comp (watch)
        (notify)
        (javac)
        (boot-test/test)))

(deftask uberjar
  []
  (comp (javac)
        (aot :namespace #{'videocapture.core})
        (uber)
        (jar)
        (target)))

(deftask appimage
  []
  (sh/sh "bash" "appimage.bash"))
