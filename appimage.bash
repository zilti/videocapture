#!/usr/bin/env bash
HERE="$(dirname "$(readlink -f "${0}")")"
# Getting tools
location="$(pwd)"
cd ~/bin
[ -f "linuxdeploy" ] || curl -L https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage -o linuxdeploy && chmod +x linuxdeploy
cd "$location"

# Build
rm -rf AppDir videocapture.AppImage input
mkdir -p AppDir/usr/share/applications
mkdir -p AppDir/usr/bin
mkdir -p AppDir/opt/videocapture
mkdir -p input/pack
cd input
curl -L https://gluonhq.com/download/javafx-13-jmods-linux/ -o javafx-jmod.zip
unzip javafx-jmod.zip
cp ../target/videocapture.jar pack
cd ..
jpackage --module-path input/javafx-jmods-* --add-modules java.base,java.sql,javafx.controls,javafx.fxml,javafx.media,javafx.swing --package-type app-image --dest jpackage-out --input input/pack --main-jar videocapture.jar --name videocapture --icon videocapture.png --java-options -Djdk.gtk.version=2
cd jpackage-out/videocapture/lib/app
cat videocapture.cfg | sed 's|APPDIR/lib|APPDIR/usr/lib|g' > videocapture.cfg.new
mv videocapture.cfg.new videocapture.cfg
# Copy jpackage result
cd "$HERE"
rm jpackage-out/videocapture/lib/runtime/lib/{libavplugin-54.so,*ffmpeg*,libglassgtk3.so}
rsync -Lr --progress jpackage-out/videocapture/* AppDir/usr
mkdir -p AppDir/usr/usr
cd AppDir/usr/usr
ln -s ../lib lib
cd "$HERE"
cd AppDir/usr/lib
ln -s runtime/lib/* .
cd "$HERE"
# Launch helpers
curl -L https://github.com/AppImage/AppImageUpdate/releases/download/continuous/appimageupdatetool-x86_64.AppImage -o AppDir/usr/bin/appimageupdatetool
chmod +x AppDir/usr/bin/appimageupdatetool
cp config.edn AppDir/opt/videocapture
export ARCH=x86_64
export UPDATE_INFORMATION="zsync|http://otrs.psy.unibe.ch/videocapture/Videocapture-x86_64.AppImage.zsync"
export SIGN=1
GST_LIBDIR="addons/gstreamer/gstreamer-1.0"
export LD_LIBRARY_PATH="addons/gstreamer:AppDir/usr/lib/runtime/lib/:AppDir/usr/lib/runtime/lib/server"
linuxdeploy --appdir AppDir --desktop-file=videocapture.desktop --icon-file=videocapture.svg --output=appimage \
	    --custom-apprun=AppimageLauncher \
	    --executable=jpackage-out/videocapture/bin/videocapture 
#rm -rf AppDir input
