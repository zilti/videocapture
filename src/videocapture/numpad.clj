(ns videocapture.numpad
  (:gen-class)
  (:require [clojurefx.fxml :as fxml]
            [clojure.java.io :as io]
            [taoensso.timbre :as timbre
             :refer [log trace debug info warn error fatal report
                     logf tracef debugf infof warnf errorf fatalf reportf
                     spy get-env]])
  (:import javafx.scene.input.KeyEvent
           javafx.scene.input.KeyCode))

(defonce force-toolkit-init (javafx.embed.swing.JFXPanel.))

(defn numpad [] (fxml/load-fxml-with-controller (io/resource (str "fxml/numpad.fxml")) "videocapture.numpad/numpad-init"))

(defn numpad-init [instance] nil)

(defn fire-key-event
  ([src tgt keycode] (fire-key-event src tgt keycode false))
  ([src tgt keycode shift?]
    (.fireEvent tgt (new KeyEvent src tgt KeyEvent/KEY_PRESSED keycode "" KeyCode/UNDEFINED shift? false false false))
    (.fireEvent tgt (new KeyEvent src tgt KeyEvent/KEY_TYPED keycode "" KeyCode/UNDEFINED shift? false false false))
    (.fireEvent tgt (new KeyEvent src tgt KeyEvent/KEY_RELEASED keycode "" KeyCode/UNDEFINED shift? false false false))))

(defn fire-key-event-special 
  ([src tgt keycode] (fire-key-event-special src tgt keycode false))
  ([src tgt keycode shift?]
    (.fireEvent tgt (new KeyEvent src tgt KeyEvent/KEY_PRESSED "" "" keycode shift? false false false))
    (.fireEvent tgt (new KeyEvent src tgt KeyEvent/KEY_TYPED "" "" keycode shift? false false false))
    (.fireEvent tgt (new KeyEvent src tgt KeyEvent/KEY_RELEASED "" "" keycode shift? false false false))))

(defn keyboard-input [instance event]
  (let [focused-field (.get (.focusOwnerProperty (.getScene (.-toplevel instance))))
        key-pressed (.getSource event)]
    (debug "Numpad: pressed" (.getId key-pressed))
    (case (.getId key-pressed)
      "keyboard_0" (fire-key-event key-pressed focused-field "0")
      "keyboard_1" (fire-key-event key-pressed focused-field "1")
      "keyboard_2" (fire-key-event key-pressed focused-field "2")
      "keyboard_3" (fire-key-event key-pressed focused-field "3")
      "keyboard_4" (fire-key-event key-pressed focused-field "4")
      "keyboard_5" (fire-key-event key-pressed focused-field "5")
      "keyboard_6" (fire-key-event key-pressed focused-field "6")
      "keyboard_7" (fire-key-event key-pressed focused-field "7")
      "keyboard_8" (fire-key-event key-pressed focused-field "8")
      "keyboard_9" (fire-key-event key-pressed focused-field "9")
      "keyboard_EG" (do (fire-key-event key-pressed focused-field "E" true)
                        (fire-key-event key-pressed focused-field "G" true))
      "keyboard_SEG" (do (fire-key-event key-pressed focused-field "S" true)
                         (fire-key-event key-pressed focused-field "E" true)
                         (fire-key-event key-pressed focused-field "G" true))
      "keyboard_SKID" (do (fire-key-event key-pressed focused-field "S" true)
                          (fire-key-event key-pressed focused-field "K" true)
                          (fire-key-event key-pressed focused-field "I" true)
                          (fire-key-event key-pressed focused-field "D" true))
      "keyboard_G" (fire-key-event key-pressed focused-field "G" true)
      "keyboard_S_TAB" (fire-key-event-special key-pressed focused-field KeyCode/TAB)
      "keyboard_S_BACKSPC" (fire-key-event-special key-pressed focused-field KeyCode/BACK_SPACE)
)))

(def keyboard-input-0 keyboard-input)
(def keyboard-input-1 keyboard-input)
(def keyboard-input-2 keyboard-input)
(def keyboard-input-3 keyboard-input)
(def keyboard-input-4 keyboard-input)
(def keyboard-input-5 keyboard-input)
(def keyboard-input-6 keyboard-input)
(def keyboard-input-7 keyboard-input)
(def keyboard-input-8 keyboard-input)
(def keyboard-input-9 keyboard-input)
(def keyboard-input-eg keyboard-input)
(def keyboard-input-seg keyboard-input)
(def keyboard-input-skid keyboard-input)
(def keyboard-input-g keyboard-input)
(def keyboard-input-tab keyboard-input)
(def keyboard-input-backspc keyboard-input)
