(ns videocapture.settings
  (:require [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.pprint :as pprint]
            [clojure.spec.alpha :as s]
            [clojure.string :as str]
            [clojurefx.clojurefx :as fx]
            [clojurefx.fxml :as fxml]
            [mount.core :as mount]
            [taoensso.timbre
             :as
             timbre
             :refer
             [debug
              debugf
              error
              errorf
              fatal
              fatalf
              get-env
              info
              infof
              log
              logf
              report
              reportf
              spy
              trace
              tracef
              warn
              warnf]]
            [videocapture.gui-util :as gui-util])
  (:import java.nio.file.Files
           (java.util.jar JarInputStream
                          JarFile)))

(declare settings)

(s/def :settings-smb/username string?)
(s/def :settings-smb/password string?)
(s/def :settings-smb/dns string?)
(s/def :settings-smb/directory string?)
(s/def :settings/smb (s/keys :req-un [:settings-smb/directory :settings-smb/dns :settings-smb/password :settings-smb/username] :opt-un []))

(s/def :settings/configuration keyword?)
(s/def :settings-form/room string?)
(s/def :settings-form/groups (s/coll-of string?))
(s/def :settings/form (s/keys :req-un [:settings-form/room] :opt-un [:settings-form/groups]))

(s/def :settings/gui string?)
(s/def :settings-camera/type #{:device :rtsp})

(s/def :settings-camera-device/address string?)

(s/def :settings-camera-rtsp/address string?)
(s/def :settings-camera-rtsp/username string?)
(s/def :settings-camera-rtsp/password string?)

(defmulti camera-type :type)
(defmethod camera-type :device [_]
  (s/keys :req-un [:settings-camera-device/address]))
(defmethod camera-type :rtsp [_]
  (s/keys :req-un [:settings-camera-rtsp/address
                   :settings-camera-rtsp/username
                   :settings-camera-rtsp/password]))

(s/def :settings/camera (s/coll-of (s/multi-spec camera-type :settings-camera/type)))

(s/def ::settings (s/keys :req-un [:settings/supervisor :settings/camera :settings/configuration :settings/gui :settings/form :settings/smb] :opt-un []))

(defn get-config-key [settings]
  {:post [(keyword? %)]}
  (get-in @settings [:configuration]))

(defn get-appversion []
  (let [jar-file-raw (io/file (.getFile (.getLocation (.getCodeSource (.getProtectionDomain videocapture.GstRenderer)))))]
    (debug "jar-file:" jar-file-raw)
    (if (.isFile jar-file-raw)
      (let [jar-file (new JarFile jar-file-raw)
            manifest (.getManifest jar-file)]
        (debug (.entrySet (.getMainAttributes manifest)))
        (info "APP-VERSION:" (.getValue (.getMainAttributes manifest) "App-Version"))
        (.getValue (.getMainAttributes manifest) "App-Version"))
      (do
        (info "APP-VERSION: Development environment.")
        "DEVEL"))))

(defn get-setting
  "Fetches a setting from the combined settings structure. The keyword `:C' gets replaced by the value of the `:configuration' variable in settings.edn."
  [top & levels]
  {:pre [(keyword? top)]}
  (let [fst (if (= top :C) [:configs (get-config-key (:settings @settings))] [top])
        {:keys [settings config]} @settings
        result (or (get-in @settings (into fst levels))
                   (get-in @config (into fst levels)))]
    (trace fst levels " => " result)
    result))

(defn update-setting "Replaces a setting with a temporary value."
  [keys value]
  (debug keys " => " value)
  (swap! (:settings @settings) update-in keys (fn [_] value)))

(defn init-settings []
  (let [confdir (if (str/blank? (System/getenv "XDG_CONFIG_HOME")) "" (str (System/getenv "XDG_CONFIG_HOME") "/"))
        optdir (if (str/blank? (System/getenv "APPDIR")) "" (str (System/getenv "APPDIR") "/opt/videocapture/"))
        settingmap (read-string (Files/readString (.toPath (io/file (str confdir "settings.edn")))))
        ;;settings (atom (s/conform ::settings settingmap))
        settings (atom settingmap)
        nonsense (debug @settings)
        ;;config (read-string (Files/readString (.toPath (io/file (str confdir "config.edn")))))
        config (read-string (Files/readString (.toPath (io/file (str optdir "config.edn")))))
        ;;config (read-string (Files/readString (.toPath (io/file "/opt/videocapture/config.edn"))))
        ]
    (swap! settings assoc :xdg {:config confdir :home (str (System/getenv "HOME") "/videocapture/")})
    (swap! settings assoc :version (get-appversion))
    (debug "Checking settings validity:" (s/valid? ::settings @settings))
    (debug "Checking settings validity, smb:" (s/valid? :settings/smb (:smb @settings)))
    (debug "Checking settings validity, configuration:" (s/valid? :settings/configuration (:configuration @settings)))
    (debug "Checking settings validity, form:" (s/valid? :settings/form (:form @settings)))
    (debug "Checking settings validity, gui:" (s/valid? :settings/gui (:gui @settings)))
    (debug "Checking settings validity, camera:" (s/valid? :settings/camera (:camera @settings)))
    (when (= @settings :clojure.spec.alpha/invalid)
      (fatal "The settings file does not conform to the spec; terminating.")
      (gui-util/alert :error "Inkorrekte Einstellungsdatei"
                      "Die Einstellungsdatei \"settings.edn\" entspricht nicht der Spezifikation. Details entnehmen Sie der Logdatei. Das Programm wird jetzt beendet.")
      (mount/stop)
      (System/exit 0))
    {:settings settings
     :config (atom config)}))

(defn destruct-settings [settings]
  nil)

(mount/defstate settings
  :start (init-settings)
  :stop (destruct-settings settings))
