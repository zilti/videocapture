(ns videocapture.gui-util
  (:require [clojurefx.clojurefx :as fx]
            [clojure.core.async :as async :refer [go chan >! <!]]
            [taoensso.timbre :as timbre
             :refer [log trace debug info warn error fatal report
                     logf tracef debugf infof warnf errorf fatalf reportf
                     spy get-env]]
            [clojure.spec.alpha :as s])
  (:import (javafx.scene.control Alert$AlertType Alert)
           (javafx.geometry Pos)
           (org.controlsfx.control Notifications)
           (javafx.scene.layout Region)))

(s/def ::type #{:information :warning :error})

(def types {:information (Alert$AlertType/INFORMATION)
	    :warning (Alert$AlertType/WARNING)
	    :error (Alert$AlertType/ERROR)})

(defn notification
  "Shows a self-closing notification popup at the bottom center of the screen."
  [type title text]
  {:pre [(s/valid? ::type type) (string? title) (string? text)]}
  (debug title text)
  (fx/run-later (let [note (.. Notifications
                               create
                               (title title)
                               (text text)
                               hideCloseButton
                               (position Pos/BOTTOM_CENTER))]
                  (case type
                    :information (.showInformation note)
                    :error (.showError note)))))

(defn alert [type title text]
  {:pre [(s/valid? ::type type) (string? title) (string? text)]}
  (debug text)
  (fx/run-now (let [alertbox (new Alert (type types))]
                (.setTitle alertbox title)
                (.setResizable alertbox true)
                (.setContentText alertbox text)
                (.setMinHeight (.getDialogPane alertbox) Region/USE_PREF_SIZE)
                (.setMinWidth (.getDialogPane alertbox) Region/USE_PREF_SIZE)
                (.. alertbox getDialogPane getScene getWindow sizeToScene)
                (.showAndWait alertbox))))

(defn unmask [instance]
  (fx/run-now (.setMouseTransparent (.-maskerPane instance) true)
              (.setOpacity (.-maskerPane instance) 0)))

(defn mask [instance & [text]] 
  (fx/run-now (.setMouseTransparent (.-maskerPane instance) false)
              (.setText (.-maskerPane instance) (or text "Bitte warten..."))
              (.setOpacity (.-maskerPane instance) 1)))
