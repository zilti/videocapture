(add-to-list 'load-path "~/.emacs.d/el-get/el-get")

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.githubusercontent.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(add-to-list 'el-get-recipe-path "~/.emacs.d/elisp/recipes")

(el-get-bundle org-mode)
(el-get-bundle s)
(el-get-bundle cider)
(el-get 'sync)


(require 'org)
(require 'ob)
(require 'ob-lob)
(require 'ob-clojure)
(require 'ob-dot)
(require 'ob-shell)
(require 's)
(require 'cider)

(setq auto-save-default nil)
(setq backup-inhibited t)

(condition-case err
    (progn
      (find-file ".nrepl-port")
      (setq nrepl-port (string-to-number (s-trim (buffer-substring-no-properties (point-min) (point-max)))))
      (kill-buffer)
      (cider-connect-clj `(:host "localhost" :port ,nrepl-port)))
  (error (cider-jack-in '())))

(while (not (try-completion "*cider-repl" (mapcar #'buffer-name (buffer-list))))
  (sleep-for 0 100))

;; (if (file-exists-p ".nrepl-port")
;;     (progn
;;       (find-file ".nrepl-port")
;;       (setq nrepl-port (string-to-number (s-trim (buffer-substring-no-properties (point-min) (point-max)))))
;;       (kill-buffer)
;;       (cider-connect-clj `(:host "localhost" :port ,nrepl-port)))
;;   (cider-jack-in '()))

(setq org-confirm-babel-evaluate nil)
(setq org-html-htmlize-output-type nil)
(setq org-babel-clojure-backend 'cider)
(org-babel-lob-ingest "libraryofbabel.org")

(defun tangle ()
  (mapc (lambda (file)
	  (find-file (expand-file-name file "."))
	  (org-babel-tangle)
	  (kill-buffer))
	(cdr argv)))

(defun weave ()
  (mapc (lambda (file)
	  (message (concat "Weaving file " file))
	  (find-file (expand-file-name file "."))
	  (org-html-export-to-html)
	  (kill-buffer)
	  
	  (message (concat "Opening exported file: " (s-replace ".org" ".html" file)))
	  (find-file (expand-file-name (s-replace ".org" ".html" file) "."))
	  (search-forward "text-table-of-contents")
	  (search-forward "<ul>")
	  (goto-char (line-end-position))
	  (insert "\n")
	  (insert "<li><a href=\"https://lyrion.ch/opensource/repositories/videocapture\">To the Repository</a></li>\n")
	  (insert "<li><a href=\"https://lyrion.ch/opensource/repositories/videocapture/doc/trunk/README.html\">0. Main Page</a></li>")
	  (save-buffer)
	  ;; (write-file (expand-file-name (s-replace ".org" ".html" file) "."))
	  (kill-buffer))
	(cdr argv)))

(if (string= (car argv) "tangle")
    (progn (message "Tangling files.")
	   (tangle))
  (progn (message "Weaving files.")
	 (weave)))
