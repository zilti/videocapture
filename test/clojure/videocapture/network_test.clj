(ns videocapture.network-test
  (:require [clojure.test :refer :all]
            [videocapture.network :refer :all]
            [mount.core :as mount]
            videocapture.settings
            [clojure.core.async :as async :refer [>! <! go chan]]
            [taoensso.timbre :as timbre
             :refer [log trace debug info warn error fatal
                     logf tracef debugf infof warnf errorf fatalf reportf
                     spy get-env]]
            [clojure.edn :as edn])
  (:import (java.net ServerSocket Socket)
           (java.io BufferedReader InputStreamReader PrintWriter OutputStream)))

(deftest fetch-camera-data
  (debug "TEST 1")
  (mount/start #'videocapture.settings/settings)
  (with-open [server-socket (ServerSocket. 8282)]
    (let [result (atom nil)]
      (go (with-open [socket (.accept server-socket)
                      reader (BufferedReader. (InputStreamReader. (.getInputStream socket)))
                      writer (PrintWriter. ^OutputStream (.getOutputStream socket) true)]
            (.println writer (pr-str {:format :camera-data :type :request :data nil}))
            (<! (async/timeout 20))
            (reset! result (edn/read-string (.readLine reader)))))
      (Thread/sleep 5)
      (mount/start #'network)
      (Thread/sleep 50)
      (is (= :camera-data (:format @result)))
      (mount/stop))))

(deftest handle-workstation
  (debug "TEST 2")
  (mount/start #'videocapture.settings/settings)
  (videocapture.settings/update-setting [:supervisor :activated] true)
  (mount/start #'network)
  (Thread/sleep 50)
  (with-open [socket (Socket. "127.0.0.1" 8282)
              reader (BufferedReader. (InputStreamReader. (.getInputStream socket)))
              writer (PrintWriter. ^OutputStream (.getOutputStream socket) true)]
    (debug (.readLine reader))
    (.println writer (pr-str {:format :camera-data :type :response
                              :data {:workstation "A001"
                                     :camera {:type :device :address "/dev/video0" :username "" :password ""}}
                              }))
    (Thread/sleep 50)
    (is (= :device (:type (:camera (first @(:sockets @network)))))))
  (mount/stop))
