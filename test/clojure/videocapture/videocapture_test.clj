(ns videocapture.videocapture-test
  (:require [clojure.test :refer :all]
            mount.core
            [videocapture.videocapture :refer :all])
  (:import (org.freedesktop.gstreamer Gst)))

(Gst/init "VideoCapture" (into-array String []))

;; (deftest pipeline
;;   (testing "Starting pipeline"
;;     (mount.core/start #'vcapture)
;;     (is (instance? org.freedesktop.gstreamer.Pipeline (:pipeline @vcapture)))
;;     (mount.core/stop #'vcapture)))

;;(Gst/deinit)
