(ns videocapture.pipeparser-test
  (:require [clojure.test :refer :all])
  (:require [videocapture.pipeparser :refer :all])
  (:import (org.freedesktop.gstreamer Gst)))

(Gst/init "VideoCapture" (into-array String []))

;(deftest helperfunction
;  (testing "conj-vec"
;    (is (= {:a [1 2 3]} (conj-vec {:a [1 2]} [:a] 3)))))

(deftest applicators
  (testing "apply-node"
    (is (= "TestNode" (.getName (:element (apply-node {:name "TestNode" :type "audiotestsrc"}))))))
  (testing "apply-settings"
    (let [node (apply-node {:name "TestNode" :type "appsink" :settings {:drop true}})]
      (apply-settings node)
      (is (= true (.get (:element node) "drop")))))
  (testing "apply-caps"
    (let [node (apply-node {:name "TestNode" :type "capsfilter" :caps "video/x-raw, format=BGRx"})]
      (apply-caps node)
      ;;(is (= "video/x-raw, format=BGRx" (.getCaps (:node node))))
      (is (= true true))))
  (testing "apply-appcap"
    (is (= true true))))

;;(Gst/deinit)